import random
from faker import Faker

faker = Faker(['pt-BR'])

nome = f'{faker.first_name()}'

sobre_nome = f'Teste Robot {faker.last_name()}'

cpf = f'{faker.cpf()}'

number_canal = f'{faker.pyint(max_value=1234568)}'

pis = f'{faker.pyint(max_value=1234564646545468)}'

nascimento = f'{faker.date(pattern="%d/%m/%Y")}'

ceps = ['69912440','78557749','82810602','75143372','49026040','86066180', '57043255', '59090657', '59135530']

ceps = random.sample(ceps,k=1)

ceps = ceps[0]

number_house =  f'{faker.pyint(max_value=1000)}'

ddd = '11'

number_phone =  f'{faker.pyint(max_value=99999999)}'

number_phone_house = f'9{faker.pyint(max_value=99999999)}'

email = f'teste@{faker.free_email_domain()}'
