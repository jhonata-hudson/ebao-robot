***Settings***
Library        SeleniumLibrary



***Keywords***  
E Realizo Entrada no Canal de vendas com dados "${canal_type}", "${like_person}"
   
    Wait Until Element Is Visible           ${enter_canal_vendas.button_add}     
   
    Click Element                           ${enter_canal_vendas.button_add}           
   
    Wait Until Element Is Visible           ${enter_canal_vendas.tipo_canal}

    Select From List By Label               ${enter_canal_vendas.tipo_canal}        ${canal_type}    

    Select From List By Label               ${enter_canal_vendas.category_vendas}   ${like_person}      
    
    Click Element                           css=${enter_canal_vendas.button_next}


Entao valido que tela "${WINDOW}" seja exibida
    
    Element Should Contain    ${basic_infos.tittle}    ${WINDOW}


Quando preencho campos obrigatorios da tela Informações de Entrada/Aprovação no Canal de vendas
    
    Write In Fields Ids 

    Click Element            css=${buttons_cadastro_pessoas.submeter}


Entao valido que mensagem "${MENSAGEM}" seja exibida 

    Element Should Contain    css=${basic_infos.aprovacao_ok}    ${MENSAGEM}

    Write in Files

    SLEEP       100000











