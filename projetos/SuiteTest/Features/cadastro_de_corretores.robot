*** Settings ***
Resource                        ../resource_main/resources_all_main.resource
Suite Setup                     Before 
Suite Teardown                  After
Variables                       ../resources/massaFaker.py



***Variables***


***Test Cases***
Cenario 01: Criar novo canal de venda
    [Documentation]     Verificar se o sistema concluí o cadastro completo de corretores corretamente
    ...                 Pré-Condição: Documento (CPF/CNPJ/CEI) da pessoa não existir no eBao
    ...                               Documento (CPF/CNPJ/CEI) deve ser válido
    ...                               Corretor deve estar cadastrado no sistema vida e possuir Código do Canal de Vendas único   
    Dado que estou logado no Ebao

    Quando Acesso o menu Canal de Vendas
   
    E Realizo Entrada no Canal de vendas com dados "Externo - individual", "Agente (Pessoa Física)"
   
    Entao valido que tela "Informações de Entrada/Aprovação no Canal de vendas" seja exibida
   
    Quando preencho campos obrigatorios da tela Informações de Entrada/Aprovação no Canal de vendas 

    Log To Console      ${nome}

    Log To Console      ${sobrenome}

    Entao valido que mensagem "A aplicação foi enviada com êxito para aprovação." seja exibida 

